var gulp = require('gulp');
var clean = require('gulp-clean');
var html = require('gulp-html-replace');
var uglify = require('gulp-uglify');
var usemin = require('gulp-usemin');
var cssmin = require('gulp-cssmin');
var autoprefixer = require('gulp-autoprefixer');

// Tarefa padrão: limpa, copia e executa minificação de JS, autoprefixação e minificaçaõ de CSS
gulp.task('default', ['copy'], function() {
  // executam as tarefas em modo assíncrono
  gulp.start('usemin');
});

// Copia arquivos de src para dist
gulp.task('copy', ['clean'], function() {
  return gulp.src('src/**/*')
      .pipe(gulp.dest('dist'));
});

// Limpa o diretório dist
gulp.task('clean', function() {
  // return garante que clean vai executar antes de copy
  return gulp.src('dist')
      .pipe(clean());
});

// Minifica JS e CSS, autoprefixa CSS
gulp.task('usemin', function() {
  gulp.src('dist/**/*.html')
      .pipe(usemin({
        'js': [uglify],
        'css': [autoprefixer, cssmin]
      }))
      .pipe(gulp.dest('dist'));
});
