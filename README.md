# Aviso Urgente - Teste Web Designer

> Lorena Calaça Ferreira

## Como executar esse projeto

Utilizei o Gulp para otimizar certos aspectos do código como minificação e autoprefixação. Os arquivos otimizados e prontos para a distribuição encontram-se no diretório `dist`.  

Para executar esse projeto e obter os arquivos otimizados, faça o seguinte:  

1. Instale o [node.js e o npm](https://nodejs.org/en/download/). Não se preocupe, o npm vem junto com o instalador do node.

2. Abra um terminal ou prompt de comando na pasta desse projeto (`aviso-urgente-teste`) e execute `npm install` para instalar as dependências (Gulp e seus plugins).

3. Ainda com o terminal aberto na raiz desse projeto, execute o comando `gulp` e espere que o mesmo execute suas tarefas.

4. Agora a estrutura do projeto deve ser algo parecido com isso:

```
|   browserslist
|   gulpfile.js
|   package.json
|   README.md
|   
+---dist
|   |   index.html
|   |   
|   +---css
|   |       app.css
|   |       font-awesome.min.css
|   |       index.min.css
|   |       materialize.min.css
|   |       
|   +---img
|   |       go.png
|   |       logo.png
|   |       
|   \---js
|           app.js
|           index.min.js
|           jquery-3.2.0.min.js
|           materialize.min.js
|           
+---node_modules
|   +--- ...
|
|                   
\---src
    |   index.html
    |   
    +---css
    |       app.css
    |       font-awesome.min.css
    |       materialize.min.css
    |       
    +---img
    |       go.png
    |       logo.png
    |       
    \---js
            app.js
            jquery-3.2.0.min.js
            materialize.min.js
```
5. Agora abra a o arquivo `index.html` da pasta `dist` e veja o resultado.

![Resultado](result.png)

6. Lembrando que todo o código legível está na pasta `src`.

## Observações

Alguns itens não ficaram idênticos ao mockup por conta das limitações do framework CSS utilizado (MaterializeCSS).

Alguns desses itens são:  

1. O posicionamento dos botões "Ações" e de paginação superiores, uma vez que a aba do Materialize não permite a adição de itens ao seu lado.  

2. O espaço entre a barra superior de navegação e o conteúdo da página, uma vez que o logo da empresa "vaza" para fora do container da `nav`. Uma solução para esse problema seria diminuir o tamanho do logo porém ele ficaria pequeno demais, logo optei pela primeira opção.  

3. Não utilizei o AngularJS aqui pois ele é um framework de JavaScript, linguagem que foi pouco usada na reprodução desse layout. Além disso, a utilização desse framework iria aumentar as horas necessárias para reproduzir o layout, logo optei por deixá-lo de lado.  